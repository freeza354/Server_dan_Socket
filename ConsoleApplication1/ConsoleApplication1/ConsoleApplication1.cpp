#include "stdafx.h"
#include <WS2tcpip.h>
#include <iostream>
using namespace std;

#pragma comment(lib, "Ws2_32.lib")

int main() {
	int result;
	WSAData wsData; // Digunakan saat membuat WSAStratup
	int DEFAULT_PORT = 5678; // Port yang digunakan

	WSAStartup(MAKEWORD(2, 2), &wsData); // Membuat winsock

	sockaddr_in hints; // Variabel untuk informasi jaringan, seperti : IP, Port
	hints.sin_family = AF_INET; // Set IPv4
	hints.sin_port = htons(DEFAULT_PORT);
	hints.sin_addr.S_un.S_addr = INADDR_ANY; // IP Bebas

	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0); // Membuat socket

	bind(sock, (SOCKADDR *)&hints, sizeof(hints)); // Salaman
	cout << "Server up..." << endl;

	cout << "Waiting client..." << endl;
	listen(sock, SOMAXCONN); // Menunggu client mengirim data

	SOCKET client = accept(sock, NULL, NULL); // Menerima client sebagai client
	cout << "Client connected...\n\n";

	closesocket(sock); // Menutup socket server, karena sudah mendapat client

	char buf[512]; // kata maks yangg diinput 512

	do {
		result = recv(client, buf, 512, 0); // menerima data dari client, jika result = 0 berarti client disconnect, jika > 0 berarti client mengirim data
		if (result > 0) {
			cout << "Data diterima: " << buf << endl;
		}
		else if (result == 0) {
			cout << "Client closing..." << endl;
		}
	} while (result > 0); // Me-loop data yang diterima dari client setiap client mengirim data

	closesocket(client); // menutup socket client, karena client disconnect
	WSACleanup(); // Menutup winsock

	return 0;
}