#include "stdafx.h"
#include <WS2tcpip.h>
#include <iostream>
#include <string>
using namespace std;

#pragma comment(lib, "Ws2_32.lib")

int main() {
	int result;
	WSAData wsData;
	string IP_SERVER = "127.0.0.1";
	int DEFAULT_PORT = 5678;

	WSAStartup(MAKEWORD(2, 2), &wsData);

	sockaddr_in hints;
	hints.sin_family = AF_INET;
	hints.sin_port = htons(DEFAULT_PORT);
	inet_pton(AF_INET, IP_SERVER.c_str(), &hints.sin_addr); // memberikan informasi IP server

	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0); // membuat socket

	connect(sock, (SOCKADDR *)&hints, sizeof(hints)); // meng-koneksi dengan server
	cout << "Connected to server..." << endl << endl;

	string text;
	cout << "Type -1 to disconnect" << endl;

	do {
		cout << "Masukkan text: ";
		cin >> text;

		send(sock, text.c_str(), 512, 0); // proses mengirim data ke server
	} while (text != "-1"); // mengirim data ke server selama teks tidak sama dengan -1

	closesocket(sock); // menutup socket client
	WSACleanup();

	return 0;
}

